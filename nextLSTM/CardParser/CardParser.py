# -*- coding: utf-8 -*-
import Card
import io

class CardParser(object):
        def __init__(self,ioFile,sampleSize,step):
                #specify newline to be } so that it reads all the way
                #to the end of the json object
                self.raw = ioFile
                self.raw.seek(0) #make sure to start at the beginning
                self.sampleSize = sampleSize
                self.step = step

        def getCard(self):
                if raw.readable():
                        return Card(raw.readline().lower(),self.sampleSize,self.step)

        def hasMoreCards(self):
                return raw.readable()

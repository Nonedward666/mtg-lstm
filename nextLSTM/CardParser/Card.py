# -*- coding: utf-8 -*-
import io

class Card(object):
    def __init__(self,jsonString,sampleSize,step):
        self.rawString = json.loads(jsonString,encoding="utf-8")
        self.attributes = dict.fromkeys([ "muid", "name", "manaCost",
                                          "effects", "types", "flavor",
                                          "rarity"], 0 )
        self.sentences = []
        self.nextChars = []

        self.sampleText()

    def getAttribute(self,attr):
        return self.attributes[attr]

    def sampleText(self,numChars,step):
        #populates sentences, and next chars.
        #sentences is a list of character vectors
        #nextChars is a list of the single character that follows the corresponding sentence
        for i in range(effectIndex,len(self.attributes["effects"])-numChars,step):
            self.sentences.append(text[i:i+numChars])
            self.nextChars.append(text[i+numChars])

    def getTextMatrix(self):
        return self.sentences

    def getResultVector(self):
        return self.nextChars

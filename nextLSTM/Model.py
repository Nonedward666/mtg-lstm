# -*- coding: utf-8 -*-
from __future__ import print_function
from keras.callbacks import LambdaCallback
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
import random
import sys
import os
import io
import codecs
import CardParser

def sample(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


def on_epoch_end(epoch, logs, log = codecs.open("log.txt","a","utf-8")):
    # Function invoked at end of each epoch.
    # Prints generated text.

    log.write(u"\n")
    log.write(u'----- Generating text after Epoch: %d' % epoch)
    print("text for epoch: %d" % epoch)
    start_index = random.randint(0, len(text) - maxlen - 1)

    for diversity in [0.2, 0.5, 1.0, 1.2]:
        log.write(u'----- diversity: ' + str(diversity))
        generated = u''
        sentence = text[start_index: start_index + maxlen]
        generated += sentence
        log.write(u'----- Generating with seed: "' + sentence + '"')
        print(u'----- Generating with seed: "' + sentence + '"')
        sys.stdout.write(generated)

        for i in range(400):
            x_pred = np.zeros((1, maxlen, len(chars)))
            for t, char in enumerate(sentence):
                x_pred[0, t, char_indices[char]] = 1.
            preds = model.predict(x_pred, verbose=0)[0]
            next_index = sample(preds, diversity)
            next_char = indices_char[next_index]
            generated += next_char
            sentence = sentence[1:] + next_char
            log.write(next_char)
            sys.stdout.write(next_char)
            sys.stdout.flush()
        log.write(u"\n")
        print()


def vectorizeIt(card, chars, sentenceLength):
    print('Vectorizing...')

    cardName = card.getAttribute("name")
    #make some dictionaries
    char_indices = dict((c, i) for i, c in enumerate(chars))
    indices_char = dict((i, c) for i, c in enumerate(chars))

    #Sentences is a list of semi reduntant character sequences of the same length
    sentences = card.getTextMatrix()

    #next chars is the character that comes immediately after it's matching character sequence 
    next_chars = card.getResultVector()

    #This is a 3 dimensional matrix,
    #individual sentences X character length of each sentence X the # unique characters in corpus
    xEffect = np.zeros((len(sentences),ESL,len(chars)),dtype=np.bool)

    #this is a 3 dimensional matrix, except it's of size of the name
    xName = np.zeros((len(sentences),NSL,len(chars)),dtype=np.bool)

    #each sentence's next char
    yChar = np.zeros((len(sentences),len(chars)),dtype=np.bool)

    #populate the numpy matrices

    #each individual sentence
    for i, sentence in enumerate(sentences):
        #each char in that sentence
        for c, char in enumerate(sentence):
            #for each char in each sentences, the corresponding character index is set to ttrue
            #so it's noted which character is in this spot
            xEffect[i, c, char_indices[char]] = 1

        #for each character in the name
        for c, char in enumerate(cardName):
            #note which character is in which spot
            xName[i,c,char_indices[char]] = 1

        #For each sentence, the character that follows
        yChar[i, char_indices[next_chars[i]]] = 1
    return xEffect,xName,yChar


def main():
    #data path
    path = "unique.json"

    #effect sample length: 40 chars
    ESL = 40
    #efffect step size: 3 chars
    STEP = 3
    #name sample length: 64 chars
    NSL = 64

    #raw character data from the file
    raw = io.open(path, encoding='utf-8')#, newline='}')

    #get unique chars, build character dictionary
    chars = sorted(list(set(raw.read().lower())))

    #card parser to handle some the text
    cardParser = CardParser(raw,ESL,STEP)

    #build the model
    model = buildModel(chars,ESL,NSL)

    #train the model
    numCards = charParser.getNumChars()
    print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

    #while there are more cards that haven't been seen
    while(cardParser.hasMoreCards()):

        #get the next card
        card = cardParser.getCard()
        print("Training on: " + card.getName())

        #put it into numbers
        xName,xEff,y = vectorizeIt(card)

        #and fit the model to it
        model.fit(xName, xEff, y,
                  batch_size = 128, #batch size
                  epochs = 60,
                  callbacks=[print_callback])

    #save the model at the end
    model.save(os.path.realpath(__file__).replace(".py",".hd5"))


def buildModel(chars,ESL,NSL):
    # build the model: a single LSTM

    #units... what does this do?
    UNITS = 128

    print('Build model...')

    #make the base model
    model = Sequential()

    #add each of the layers
    model.add(LSTM(UNITS, input_shape=(ESL, NSL, len(chars))))
    model.add(Dense(len(chars)))
    model.add(Activation('softmax'))

    optimizer = RMSprop(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)
    print("Model built...")
    return model





if __name__ == "__main__":
    print("starting...")
    main()

import json

def main():
	avg = dict.fromkeys(["muid","name","manaCost","effects","types","flavor","rarity"], 0 )
	maxxx = dict.fromkeys(["muid","name","manaCost","effects","types","flavor","rarity"], 0 )
	total = dict.fromkeys(["muid","name","manaCost","effects","types","flavor","rarity"], 0 )
	log = open("unique.json","r")
	i = 0
	tc = 0
	for line in log:
#		print "running..."
		card = json.loads(line.decode('utf-8'),encoding="utf-8")
		analyzeFields(avg,maxxx,total,card,i)
		i += 1
		tc += len(line)

	print "avg"
	printResults(avg)
	print "\nMaxxx"
	printResults(maxxx)
	print "\ntotal"
	printResults(total)

def analyzeField(avg,maxxx,total,field,card,i):
	val = unicode(card[field])
	if len(val) > maxxx[field]:
		maxxx[field] = len(val)
	avg[field] = ((avg[field]*i) + len(val)) / (i+1)
	total[field] += len(val)

def analyzeFields(avg,maxxx,total,card,i):
	for field in card:
		analyzeField(avg,maxxx,total,field,card,i)

def printResults(d):
	for k in d:
		print k + " " + str(d[k])

if __name__ == "__main__":
#	print "starting..."
	main()
